﻿using System;
using System.IO;
using System.Collections.Generic;

namespace ion
{

    class Program
    {
        static void Main(string[] args)
        {
            if(args.Length > 0)
            {
                compile(File.ReadAllText(args[0]));
            }
            else
            {
                Console.WriteLine("No file provided");
            }
        }

        static void compile(string inputCode)
        {

            string[] cleanCode = inputCode.Split(new char[] { '\t', '\r', '\f', '\v','\\' }, StringSplitOptions.RemoveEmptyEntries); //to clear up confusion
            string code = "";
            for(int i = 0; i < cleanCode.Length; i++)
            {
                code += cleanCode[i];
            }

            string[] lines = code.Split("\n");
            
            string currentFile = "";
            Dictionary<string, int> variables = new Dictionary<string, int>();
            string currentVariable = "";
            int fileValue = 0;
            
            bool abort = false;

            for(int i = 0; i < lines.Length; i++)
            {
                string[] commands;
                if(lines[i].Contains(" "))
                    commands = lines[i].Split(" ");
                else
                    commands = new string[] { lines[i] };

                if(commands[0] == "^") //^ [file path]
                {
                    //open
                    if(commands.Length > 1)
                    {
                        currentFile = commands[1];
                        if(File.Exists(currentFile)) //you CAN open files that don't exist (just setting the currentFile value)
                        {
                            string file = File.ReadAllText(currentFile);
                            if(!int.TryParse(file, out fileValue))
                                Console.WriteLine(currentFile + " does not contain an integer value");
                        }
                    }
                    else
                        abort = true;
                }
                if(commands[0] == ".") //. [value]
                {
                    //write
                    if(currentFile != "")
                        if(commands.Length == 2)
                        {
                            int saveValue = 0;

                            if(!int.TryParse(commands[1], out saveValue)) //if its an int this will do its thing
                            {
                                if(commands[1] == "$") //whats the point?
                                    saveValue = fileValue; //whatever, do it anyway I guess
                                if(commands[1].StartsWith("#")) //we're saving a variable
                                {
                                    string varName = commands[1].Remove(0, 1);
                                    if(varName != "")
                                        if(!variables.ContainsKey(varName))
                                            abort = true;
                                        else
                                        {
                                            saveValue = variables[varName];
                                            currentVariable = varName;
                                        }
                                    else
                                        if(currentVariable != "")
                                        {
                                            varName = currentVariable;
                                            saveValue = variables[varName];
                                        }
                                        else
                                            abort = true;
                                }
                            }
                            File.WriteAllText(currentFile, saveValue.ToString());
                        }
                    else
                        abort = true;
                }
                if(commands[0].StartsWith("#")) //#[new variable name]
                {
                    //set current variable
                    if(commands.Length == 1)
                    {
                        string varName = commands[0].Remove(0, 1);
                        if(varName != "") //if it's just the current variable it's meaningless
                        {
                            if(!variables.ContainsKey(varName))
                                variables.Add(varName, 0); //create it if it doesn't exist
                            currentVariable = varName;
                        }
                    }
                    else
                        abort = true;
                }
                if(commands[0] == "+") //+ [value] [addition value]
                {
                    //add
                    if(commands.Length == 3)
                    {
                        if(commands[1].StartsWith("#"))
                        {
                            string varName = commands[1].Remove(0, 1);
                            if(varName != "")
                            {
                                if(!variables.ContainsKey(varName))
                                    variables.Add(varName, 0);
                                currentVariable = varName; //we've changed the current variable
                            }
                            else
                                if(currentVariable != "")
                                    varName = currentVariable;

                            if(varName != "")
                            {
                                int addValue = 1;

                                
                                if(!int.TryParse(commands[2], out addValue))
                                {
                                    if(commands[2].StartsWith("#"))
                                    {
                                        string aVarName = commands[2].Remove(0, 1);
                                        if(aVarName != "")
                                        {
                                            if(variables.ContainsKey(aVarName))
                                                addValue = variables[aVarName];
                                            else
                                                abort = true;
                                        }
                                        else
                                            addValue = variables[currentVariable];
                                    }
                                    else if(commands[2] == "$")
                                        addValue = fileValue;
                                    else
                                        abort = true;
                                }

                                variables[varName] += addValue;

                            }
                            else
                                abort = true;
                        }
                        else
                            abort = true;
                    }
                    else
                        abort = true;
                }
                if(commands[0] == "-") //- [value] [subtraction value]
                {
                    //subtract
                    if(commands.Length == 3)
                    {
                        if(commands[1].StartsWith("#"))
                        {
                            string varName = commands[1].Remove(0, 1);
                            if(varName != "")
                            {
                                if(!variables.ContainsKey(varName))
                                    variables.Add(varName, 0);
                                currentVariable = varName; //we've changed the current variable
                            }
                            else
                                if(currentVariable != "")
                                    varName = currentVariable;

                            if(varName != "")
                            {
                                int subtractValue = 1;

                                
                                if(!int.TryParse(commands[2], out subtractValue))
                                {
                                    if(commands[2].StartsWith("#"))
                                    {
                                        string sVarName = commands[2].Remove(0, 1);
                                        if(sVarName != "")
                                        {
                                            if(variables.ContainsKey(sVarName))
                                                subtractValue = variables[sVarName];
                                            else
                                                abort = true;
                                        }
                                        else
                                            subtractValue = variables[currentVariable];
                                    }
                                    else if(commands[2] == "$")
                                        subtractValue = fileValue;
                                    else
                                        abort = true;
                                }

                                variables[varName] -= subtractValue;

                            }
                            else
                                abort = true;
                        }
                        else
                            abort = true;
                    }
                    else
                        abort = true;
                }
                if(commands[0] == "=") //= [value] [desired value]
                {
                    //equals
                    if(commands.Length == 3)
                    {
                        if(commands[1].StartsWith("#")) //its a variable (good)
                        {
                            string varName = commands[1].Remove(0, 1); //the actual name
                            if(varName != "") //it is a specific variable
                            {
                                if(!variables.ContainsKey(varName))
                                    variables.Add(varName, 0); //add the variable since it doesn't exist
                                currentVariable = varName; //current variable has changed
                            }
                            else //we're using the last used variable
                                if(currentVariable != "")
                                    varName = currentVariable; //we can use the last used variable

                            if(varName != "") //theres a valid variable
                            {
                                int equalValue = variables[varName];

                                if(!int.TryParse(commands[2], out equalValue)) //if it's an int it will become the equal value
                                {
                                    //if it couldn't parse it's probably a variable name
                                    if(commands[2].StartsWith("#")) //it is a variable name!
                                    {
                                        string eqVarName = commands[2].Remove(0, 1);
                                        if(eqVarName != "")
                                        {
                                            if(variables.ContainsKey(eqVarName))
                                                equalValue = variables[eqVarName]; //its a valid variable, so we set var1 equal to it
                                            else
                                                abort = true;
                                        }
                                        //otherwise we do nothing and this whole thing has been pointless
                                    }
                                    else if(commands[2] == "$") //file value
                                        equalValue = fileValue; //equal value becomes file value
                                    else
                                        abort = true;
                                }

                                variables[varName] = equalValue; //finally! set equal
                            }
                            else
                                abort = true;

                        }
                        else
                            abort = true;
                    }
                    else
                        abort = true;
                }
                if(commands[0] == "?") //? [value 1] [operator] [value 2] :
                {
                    //if statement
                    if(commands.Length == 5)
                    {
                        int firstValue = 0;
                        int secondValue = 0;

                        bool isTrue = false;

                        //start with the first value
                        if(!int.TryParse(commands[1], out firstValue))
                        {
                            if(commands[1].StartsWith("#"))
                            {
                                string varName = commands[1].Remove(0, 1);
                                if(varName != "")
                                {
                                    if(variables.ContainsKey(varName))
                                        firstValue = variables[varName];
                                    else
                                        abort = true;
                                }
                                else
                                    if(currentVariable != "")
                                        varName = currentVariable;
                                    else
                                        abort = true;
                            }
                            else if(commands[1] == "$")
                                firstValue = fileValue;
                            else
                                abort = true;
                        }

                        //finally the second value
                        if(!int.TryParse(commands[3], out secondValue))
                        {
                            if(commands[3].StartsWith("#"))
                            {
                                string varName = commands[3].Remove(0, 1);
                                if(varName != "")
                                {
                                    if(variables.ContainsKey(varName))
                                        secondValue = variables[varName];
                                    else
                                        abort = true;
                                }
                                else
                                    if(currentVariable != "")
                                        varName = currentVariable;
                                    else
                                        abort = true;
                            }
                            else if(commands[3] == "$")
                                secondValue = fileValue;
                            else
                                abort = true;
                        }

                        //and the operator
                        switch(commands[2])
                        {
                            case "~":
                                if(firstValue == secondValue)
                                    isTrue = true;
                                break;
                            case ">":
                                if(firstValue > secondValue)
                                    isTrue = true;
                                break;
                            case "<":
                                if(firstValue < secondValue)
                                    isTrue = true;
                                break;
                            default:
                                abort = true;
                                break;
                        }

                        if(isTrue)
                            Console.WriteLine("TRUE");
                        else
                            Console.WriteLine("FALSE");

                    }
                    else
                        abort = true;
                }

                if(abort)
                {
                    Console.WriteLine("An error occurred at line " + i);
                    break;
                }

            }

            Console.WriteLine("Execution complete");

        }

    }
}
