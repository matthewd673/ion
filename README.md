# ion •
A simple, not-super-useful interpretted programming language. Originally forked from the even more esoteric [matthewd673/complexion]()
## Features
* Designed for shorthand (adding is as simple as `+ # 5`)
* Addition and subtraction
* Loading and writing to files
* Variables (integers)
## Writing
Ion revolves around a few one character commands that each perform key tasks. Each of these lines is contained in a line on the document.
##Line-by-line analysis of `test.ion`
1. Open `1.txt`
2. Create variable `var`
3. Set `var` equal to the contents of `1.txt`
4. Add `1` to `var`'s value
5. Open `2.txt` *(it will be created if it does not exist)*
6. Write `var` to `2.txt`
7. Open `3.txt`
8. Create variable `alsovar`
9. Set `alsovar` equal to `100`
10. Subtract `var` from `alsovar`
11. Write `alsovar` to `3.txt`
